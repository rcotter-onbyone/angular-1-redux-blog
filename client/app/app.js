import angular      from 'angular';
import uiRouter     from 'angular-ui-router';
import ngRedux      from 'ng-redux';
import ReduxThunk   from 'redux-thunk';
import ReduxLogger  from 'redux-logger';
import perflogger   from 'redux-perf-middleware';

import AppComponent from './app.component';
import ConfirmComponent from './components/confirm/confirm';
import MemberTableComponent from './components/member-table/member-table';
import TeamPageComponent from './containers/team-page/team-page';

import {RootReducer} from './reducers';
import {DevToolsConfig} from './dev-tools-config';

import 'normalize.css';

angular
  .module('app', [
    uiRouter,
    ngRedux,
    ConfirmComponent.name,
    MemberTableComponent.name,
    TeamPageComponent.name
  ])
  .config(($locationProvider, $stateProvider, $urlRouterProvider, $ngReduxProvider) => {
    "ngInject";

    $stateProvider
      .state('app', {
        url: '',
        abstract: true,
        template: '<app></app>'
      })
      .state('app.team-page', {
        url: '/team-page',
        template: '<team-page></team-page>'
      });

    $urlRouterProvider.otherwise('/team-page');

    $ngReduxProvider.createStoreWith(
      RootReducer,
      [
        ReduxThunk,
        ReduxLogger(),
        perflogger
      ],
      [
        window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__({
          getMonitor: (monitor) => { DevToolsConfig.devToolsIsPlayingBack = monitor.isMonitorAction; }
        }) : f => f
      ]
    );
  })
  .run(['$ngRedux', '$rootScope', '$timeout', ($ngRedux, $rootScope, $timeout) => {
    //To reflect state changes when disabling/enabling actions via the monitor
    //there is probably a smarter way to achieve that
    $ngRedux.subscribe(() => {
      $timeout(() => {$rootScope.$apply(() => {})}, 100);
    });
  }])
  .component('app', AppComponent);
