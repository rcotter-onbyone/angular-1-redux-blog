import Immutable from 'immutable';
import randomName from 'node-random-name';
import uuid from 'node-uuid';

import {TEAM} from '../constants/team';

function createMember(notify = true) {
  return {
    id: uuid.v4(),
    name: randomName({random: Math.random}),
    selected: false,
    notify: notify
  }
}

const member1 = createMember(false);
const member2 = createMember(false);
const member3 = createMember(false);
const members = {};
members[member1.id] = member1;
members[member2.id] = member2;
members[member3.id] = member3;

const initialState = Immutable.fromJS({
  name: 'My Team',
  inProgress: false,
  confirm: false,
  selectedCount: 0,
  addingCount: 0,
  members: members
});

export default function TeamReducer(state = initialState, action) {
  switch (action.type) {

    case TEAM.MEMBER_SELECTED:
      return state.merge({
        selectedCount: state.get('selectedCount') + (action.selected ? 1 : -1),
        members: state.get('members').setIn([action.member.get('id'), 'selected'], action.selected)
      });


    case TEAM.MEMBERS_ALL_SELECTED:
      const members = state.get('members');
      const allSelected = state.get('selectedCount') == members.count();
      return state.merge({
        selectedCount: (allSelected ? 0 : members.count()),
        members: members.map(m => m.set('selected', !allSelected))
      });


    case TEAM.ADDING_MEMBER:
      return state.merge({
        inProgress: false,
        addingCount: state.get('addingCount') + 1
      });


    case TEAM.MEMBER_ADDED:
      const member = createMember();
      const addingCount = state.get('addingCount') - 1;
      return state.merge({
        inProgress: 0 < addingCount,
        addingCount: addingCount,
        members: state.get('members').set(member.id, Immutable.fromJS(member))
      });


    case TEAM.REMOVE_MEMBERS_REQUIRES_CONFIRMATION:
      return state.set('confirm', true);


    case TEAM.REMOVE_MEMBERS_CANCELLED:
      return state.set('confirm', false);


    case TEAM.REMOVING_MEMBERS:
      return state.merge({
        confirm: false,
        inProgress: true
      });


    case TEAM.MEMBERS_REMOVED:
      return state.merge({
        inProgress: false,
        selectedCount: 0,
        members: state.get('members').filterNot(m => m.get('selected'))
      });

    default:
      return state;
  }
}
