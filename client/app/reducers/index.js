import {combineReducers} from 'redux';
import TeamReducer from './team.reducer';

export const RootReducer = combineReducers({
  team: TeamReducer
});