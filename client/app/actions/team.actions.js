import {TEAM} from '../constants/team';
import {DevToolsConfig} from '../dev-tools-config';

function selectMember(member, selected) {
  return {
    type: TEAM.MEMBER_SELECTED,
    member: member,
    selected: selected
  }
}

function selectAllMembers() {
  return {
    type: TEAM.MEMBERS_ALL_SELECTED
  }
}

function addMember() {
  return function (dispatch) {
    dispatch(addingMember())

    setTimeout(
      () => {
        dispatch(_memberAdded())
      },
      3000
    );
  };
}

function addingMember() {
  return {
    type: TEAM.ADDING_MEMBER
  }
}

function _memberAdded() {
  return {
    type: TEAM.MEMBER_ADDED
  }
}

function removeMembersRequiresConfirmation() {
  return {
    type: TEAM.REMOVE_MEMBERS_REQUIRES_CONFIRMATION
  }
}

function removeMembersCancelled() {
  return {
    type: TEAM.REMOVE_MEMBERS_CANCELLED
  }
}

function removeMembers() {
  return function (dispatch) {
    dispatch(_removingMembers());

    if (DevToolsConfig.devToolsIsPlayingBack()) {
      return dispatch(_membersRemoved());
    }

    setTimeout(
      () => {
        dispatch(_membersRemoved())
      },
      3000
    );
  };
}

function _removingMembers() {
  return {
    type: TEAM.REMOVING_MEMBERS
  }
}

function _membersRemoved() {
  return {
    type: TEAM.MEMBERS_REMOVED
  }
}

export default {
  selectMember,
  selectAllMembers,
  addMember,
  removeMembersRequiresConfirmation,
  removeMembersCancelled,
  removeMembers
};