import controller from './confirm.controller';

let confirmComponent = {
  restrict: 'E',
  bindings: {},
  controller
};

export default confirmComponent;
