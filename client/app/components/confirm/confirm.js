import confirmComponent from './confirm.component';

export default angular
    .module('confirm', [])
    .component('confirm', confirmComponent);
