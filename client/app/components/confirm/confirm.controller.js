import TeamActions from '../../actions/team.actions';
import {DevToolsConfig} from '../../dev-tools-config';

class ConfirmController {
  constructor($ngRedux) {
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, TeamActions)(this);
  }

  $onInit() {
    if (DevToolsConfig.devToolsIsPlayingBack()) {
      return this.removeMembersCancelled();
    }

    if (confirm("You sure you want to remove these members?")) {
      this.removeMembers();
    } else {
      this.removeMembersCancelled();
    }
  }

  $onDestroy() {
    this.unsubscribe();
  }

  mapStateToThis(state) {
    return {};
  }
}

ConfirmController.$inject = ['$ngRedux'];

export default ConfirmController;
