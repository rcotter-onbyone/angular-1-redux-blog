import template from './member-table.html';
import controller from './member-table.controller';
import './member-table.scss';

let memberTableComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default memberTableComponent;
