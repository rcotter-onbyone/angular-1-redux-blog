import memberTableComponent from './member-table.component';

export default angular
  .module('member-table', [])
  .component('memberTable', memberTableComponent);
