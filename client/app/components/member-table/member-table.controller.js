import TeamActions from '../../actions/team.actions';

class MemberTableController {
  constructor($ngRedux) {
    this.members = [];
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, TeamActions)(this);
  }

  orderByName(member) {
    return member.get('name');
  }

  checkClicked(member) {
    this.selectMember(member, !member.get('selected'));
  }

  allSelectedClicked() {
    this.selectAllMembers();
  }

  $onDestroy() {
    this.unsubscribe();
  }

  mapStateToThis(state) {
    return {
      inProgress: state.team.get('inProgress'),
      members: state.team.get('members').toArray(),
      allSelected: state.team.get('members').count() == state.team.get('selectedCount')
    };
  }
}

MemberTableController.$inject = ['$ngRedux'];

export default MemberTableController;
