import teamPageComponent from './team-page.component';

export default angular
  .module('team-page', [])
  .component('teamPage', teamPageComponent);
