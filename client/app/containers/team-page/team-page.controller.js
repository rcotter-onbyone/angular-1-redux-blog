import TeamActions from '../../actions/team.actions';

class TeamPageController {
  constructor($ngRedux) {
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, TeamActions)(this);
  }

  $onInit() {
  }

  $onDestroy() {
    this.unsubscribe();
  }

  addMemberClicked($event) {
    $event.stopPropagation(); // Prevent double clicks
    this.addMember()
  }

  removeMembersClicked() {
    this.removeMembersRequiresConfirmation();
  }

  mapStateToThis(state) {
    return {
      teamName: state.team.get('name'),
      addingCount: state.team.get('addingCount'),
      selectedCount: state.team.get('selectedCount'),
      confirm: state.team.get('confirm'),
      inProgress: state.team.get('inProgress')
    };
  }
}

TeamPageController.$inject = ['$ngRedux'];

export default TeamPageController;
