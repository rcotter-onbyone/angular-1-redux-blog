import template from './team-page.html';
import controller from './team-page.controller';
import './team-page.scss';

let teamPageComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default teamPageComponent;
