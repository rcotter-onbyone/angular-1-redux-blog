export const TEAM = {
  MEMBER_SELECTED: 'MEMBER_SELECTED',
  MEMBERS_ALL_SELECTED: 'MEMBERS_ALL_SELECTED',
  ADDING_MEMBER: 'ADDING_MEMBER',
  MEMBER_ADDED: 'MEMBER_ADDED',
  REMOVE_MEMBERS_REQUIRES_CONFIRMATION: 'REMOVE_MEMBERS_REQUIRES_CONFIRMATION',
  REMOVE_MEMBERS_CANCELLED: 'REMOVE_MEMBERS_CANCELLED',
  REMOVING_MEMBERS: 'REMOVING_MEMBERS',
  MEMBERS_REMOVED: 'MEMBERS_REMOVED'
};

